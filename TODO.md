# TODO

---

* Calculate screen width dynamically and generate flex-div components on the fly after receiving any data so images are tightly aligned to eachother.
* Make /login and /signup their own routes, so we can guard against specific routes with auth, instead of having each component deal with checking if a user session is valid
* Better pagination strategy - at the very least a way to jump to a certain page and to specify number of images to display
* ...
* Tests :(
