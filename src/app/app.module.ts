import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { ClickOutsideModule } from 'ng-click-outside';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchComponent } from './components/navigation/search/search.component';
import { ProfileComponent } from './components/navigation/profile/profile.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { LoginComponent } from './components/navigation/login/login.component';
import { ImagesComponent } from './components/images/images.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FavoriteComponent } from './components/images/favorite/favorite.component';
import { SignupComponent } from './components/navigation/signup/signup.component';
import { SignupService } from './services/signup.service';
import { LoginService } from './services/login.service';
import { SessionService } from './services/session.service';
import { CookieService } from 'ngx-cookie-service';
import { AutosizeModule } from 'ngx-autosize';


@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    ProfileComponent,
    NavigationComponent,
    LoginComponent,
    ImagesComponent,
    FavoriteComponent,
    SignupComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ClickOutsideModule,
    AutosizeModule,
  ],
  providers: [
    LoginService,
    SignupService,
    CookieService,
    SessionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
