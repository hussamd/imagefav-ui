import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginService } from './login.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private active: boolean;
  private timeoutTask: any;
  private sessionUrl = `${environment.imagefavUrl}/session`;

  constructor(private loginSvc: LoginService,
              private http: HttpClient) {
    this.timeoutTask = null;
    this.active = false;
    // Keep track of login/logout events
    this.loginSvc.loginAlert$.subscribe(
      (successful) => {
        this.active = successful;
      }
    );

    // Initiate first session check
    this.checkSession();
  }

  // scheduleSessionChecker() is a convenience method to
  // launch a new setTimeout() task for validating sessions
  // on a scheduled timer.
  private scheduleSessionChecker() {
    if (!this.timeoutTask) {
      this.timeoutTask = setTimeout(
        () => this.checkSession(),
        60000
      );
    }
  }

  // checkSession() validates via Session Service whether the
  // current session is still active.
  //
  // The response is sent to a proper stream active/inactive
  // stream.
  checkSession() {
    this.http.get(this.sessionUrl).subscribe(
      () => {
        this.active = true;
        this.loginSvc.loginSuccessful(this.active);
      },
      () => {
        this.active = false;
        this.loginSvc.loginSuccessful(this.active);
      }
    );
    this.timeoutTask = null;
    this.scheduleSessionChecker();
  }

  isActive(): boolean {
    return this.active;
  }
}
