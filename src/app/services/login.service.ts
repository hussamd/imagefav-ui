import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { HeaderService } from './header.service';
import { environment } from '../../environments/environment';
import { Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class LoginService {
    private loginReq = new Subject<boolean>();
    private successfulLogin = new Subject<boolean>();

    loginRequests$ = this.loginReq.asObservable();
    loginAlert$ = this.successfulLogin.asObservable();

    loginUrl = `${environment.imagefavUrl}/login`;
    logoutUrl = `${environment.imagefavUrl}/logout`;

    // Constructor
    constructor(private http: HttpClient,
                private cookieSvc: CookieService,
                private headerSvc: HeaderService) {}

    // logIn()
    login(username: string, password: string): Observable<any> {
        return this.http.post(
            `${this.loginUrl}` + '/web',
            JSON.stringify({username, password}),
            {
                withCredentials: true,
            }
        ).pipe(
            tap(
                () => { this.loginSuccessful(true); }
            )
        );
    }

    // logout()
    logout() {
        const options = {
            headers: this.headerSvc.authHeaders(),
            withCredentials: true,
        };
        this.http.post(`${this.logoutUrl}` + '/web', options).subscribe();
        this.loginSuccessful(false);
    }

    // loginSuccessful() - emit a login successful event
    loginSuccessful(val: boolean) {
        this.successfulLogin.next(val);
    }

    // loginRequest() - emit a login requested event
    loginRequest(val: boolean) {
        this.loginReq.next(val);
    }
}
