import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SignupService {
  private signupReq = new Subject<boolean>();
  private signupUrl = `${environment.imagefavUrl}/register`;

  signupRequests$ = this.signupReq.asObservable();

  constructor(private http: HttpClient) { }

  signupRequest(val: boolean) {
    this.signupReq.next(val);
  }

  register(name: string, email: string, username: string, password: string): Observable<any> {
    return this.http.post(this.signupUrl, JSON.stringify({
      name, email, username, password
    }));
  }
}
