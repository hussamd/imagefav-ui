import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable, Subscription, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService implements OnDestroy {
  private searchOutput = new Subject<any>();

  private searchUrl = `${environment.imagefavUrl}/search`;

  searchReturned$ = this.searchOutput.asObservable();
  subscription: Subscription;

  constructor(private http: HttpClient) { }

  search(term: string, limit: number = 10, offset: number = 0): Observable<any> {
    const options = {
      params: new HttpParams()
        .set('q', term)
        .set('limit', limit.toString())
        .set('offset', offset.toString()),
      withCredentials: true,
    };

    const obs = this.http.get(this.searchUrl, options);
    this.searchOutput.next(obs);
    return obs;
   }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
