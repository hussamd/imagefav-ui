import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { LoginService } from './login.service';
import { HeaderService } from './header.service';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FavoriteService {
  private favUrl = `${environment.imagefavUrl}/favorites`;
  private queryUrl = `${environment.imagefavUrl}/query/favorites`;

  constructor(private http: HttpClient,
              private loginSvc: LoginService,
              private headerSvc: HeaderService) {}

  private httpOptions(): any {
    return {
      headers: this.headerSvc.authHeaders(),
      withCredentials: true,
    };
  }

  // add() favorites an image
  add(imageID: string): Observable<any> {
    return this.http.post(
        this.favUrl,
        JSON.stringify({image: imageID}),
        this.httpOptions(),
    );
   }

  // remove() unfavorites an image
  remove(imageID: string): Observable<any> {
    return this.http.delete(
      this.favUrl + `/${imageID}`,
      this.httpOptions(),
    );
  }

  // areFavorites() checks the given list of images to see
  // if they have been favorited previously
  areFavorites(images: string[]): Observable<any> {
    const options = {
      headers: this.headerSvc.authHeaders(),
      withCredentials: true,
    };

    return this.http.post(
      this.queryUrl,
      JSON.stringify({images}),
      options,
    );
  }
}
