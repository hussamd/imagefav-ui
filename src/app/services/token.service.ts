import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor(private cookieSvc: CookieService) { }

  getToken(): string {
    return this.cookieSvc.get('session');
  }
}
