import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TagsService {
  private baseUrl = environment.imagefavUrl;
  private tagsChanged$ = new BehaviorSubject({});
  currentTags = this.tagsChanged$.asObservable();

  constructor(private http: HttpClient) { }

  add(imageId: string, tag: string): Observable<any> {
    return this.http.post(
      `${this.baseUrl}/favorites/${imageId}/tags`,
      JSON.stringify({tag})
    );
  }

  remove(imageId: string, tag: string): Observable<any> {
    return this.http.delete(
      `${this.baseUrl}/favorites/${imageId}/tags/${tag}`
    );
  }

  tagsChange(imageId: string, tags: string[]) {
    this.tagsChanged$.next({
      [imageId]: tags
    });
  }
}
