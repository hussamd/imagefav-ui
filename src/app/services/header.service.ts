import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  constructor(private tokenSvc: TokenService) { }

  authHeaders(): HttpHeaders {
    const headers = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('X-Auth-Token',  this.tokenSvc.getToken());

    return headers;
  }
}
