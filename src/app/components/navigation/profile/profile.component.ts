import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoginService } from '../../../services/login.service';
import { SignupService } from '../../../services/signup.service';
import { SessionService } from '../../../services/session.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {
  loggedIn: boolean;
  private loginSubscription: Subscription;
  private loginReqSubscription: Subscription;
  private loginWindowShowing = false;
  private signupWindowShowing = false;

  constructor(private loginSvc: LoginService,
              private signupSvc: SignupService,
              private sessionSvc: SessionService) {
  }

  private hideSignIn() {
    this.loginWindowShowing = false;
    this.loginSvc.loginRequest(false);
  }

  private hideSignUp() {
    this.signupWindowShowing = false;
    this.signupSvc.signupRequest(false);
  }

  showSignIn() {
    this.loginSvc.loginRequest(true);
    this.loginWindowShowing = true;
    this.hideSignUp();
  }

  showSignUp() {
    this.signupSvc.signupRequest(true);
    this.signupWindowShowing = true;
    this.hideSignIn();
  }

  logout() {
    this.loginSvc.logout();
  }

  onBlur(event) {
    if (this.loginWindowShowing) {
      this.hideSignIn();
    }
    if (this.signupWindowShowing) {
      this.hideSignUp();
    }
  }

  ngOnInit() {
    this.loginSubscription = this.loginSvc.loginAlert$.subscribe(
      (value) => {
        this.loggedIn = value;
        if (this.loggedIn) {
          this.loginWindowShowing = false;
          this.signupWindowShowing = false;
        }
      }
    );

    this.loginReqSubscription = this.loginSvc.loginRequests$.subscribe(
      (value) => {
        this.loginWindowShowing  = value;
      }
    );

    this.loggedIn = this.sessionSvc.isActive();
  }

  ngOnDestroy() {
    this.loginSubscription.unsubscribe();
    this.loginReqSubscription.unsubscribe();
  }
}
