import { Component } from '@angular/core';
import { SearchService } from '../../../services/search.service';
import { FormGroup, FormControl } from '@angular/forms';

import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent {
  searchForm = new FormGroup({
    searchTerm: new FormControl('')
  });

  constructor(private searchSvc: SearchService) { }

  onSubmit() {
    const term = this.searchForm.get('searchTerm').value;
    if (!term) {
      return;
    }
    if (term.replace(/ /g, '') === '') {
      this.searchForm.reset();
      return;
    }
    this.searchSvc.search(term);
  }
}
