import { Component, OnInit, OnDestroy } from '@angular/core';
import { SignupService } from '../../../services/signup.service';
import { LoginService } from '../../../services/login.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit, OnDestroy {
  private show: boolean;
  private signupSubscription: Subscription;
  submitted = false;
  signupError: string;
  signupForm: FormGroup;

  constructor(private signupSvc: SignupService, private loginSvc: LoginService) { }

  toggleShow(show?: boolean) {
    if (show !== undefined) {
      this.show = show;
    } else {
      this.show = this.show ? false : true;
    }

    this.signupForm.reset();
  }

  private login(username: string, password: string) {
    this.loginSvc.login(username, password).subscribe(
      () => {
        this.toggleShow();
      }
    );
  }

  onSubmit() {
    this.submitted = true;
    this.signupSvc.register(
      this.name.value,
      this.email.value,
      this.username.value,
      this.password.value,
    ).subscribe(
      (res) => {
        this.login(this.username.value, this.password.value);
        this.signupForm.reset();
      },
      (res) => {
        console.log(res);
        this.signupError = res.error.description;
        this.repassword.reset();
        this.password.reset();
        if (res.status >= 500) {
          this.signupError = 'Something went wrong, please try again';
        }
      }
    );
  }

  get username() {
    return this.signupForm.get('username');
  }

  get password() {
    return this.signupForm.get('password');
  }

  get repassword() {
    return this.signupForm.get('password2');
  }

  get name() {
    return this.signupForm.get('name');
  }

  get email() {
    return this.signupForm.get('email');
  }

  matchPasswords(group: FormGroup) {
    const pass = this.password;
    const repass = this.repassword;

    return pass === repass ? null : { notEqual: true };
  }

  ngOnInit() {
    // Listen from anywhere in the app for events to sign up
    this.signupSubscription = this.signupSvc.signupRequests$.subscribe(
      (val) => this.toggleShow(val)
    );

    // Register a signup form
    this.signupForm = new FormGroup({
      name: new FormControl(
        '', [Validators.required, Validators.minLength(2), Validators.maxLength(255)]
      ),
      email: new FormControl(
        '', [Validators.required, Validators.email]
      ),
      username: new FormControl(
        '', [Validators.required, Validators.minLength(2)]
      ),
      password: new FormControl(
        '', [Validators.required, Validators.minLength(8)]
      ),
      password2: new FormControl(
        '', [Validators.required, Validators.minLength(8)]
      )
    });

    this.signupForm.valueChanges.subscribe(
      () => { this.submitted = false; },
    );
  }

  ngOnDestroy() {
    this.signupSubscription.unsubscribe();
  }
}
