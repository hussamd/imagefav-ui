import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { LoginService } from '../..//../services/login.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';

// Open a login window and handle auth related activities
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnDestroy, OnInit {
  private loginSubscription: Subscription;
  show: boolean;
  loginForm: FormGroup;
  submitted: boolean;
  loginError: string;

  constructor(private loginSvc: LoginService) {}

  onSubmit() {
    // Validate
    this.submitted = true;
    if (!this.username.valid || !this.password.valid) {
      return;
    }

    // Submit
    this.loginSvc.login(
      this.username.value,
      this.password.value
    ).subscribe(
      () => {
        this.toggleShow();
      },
      (res) => {
        if (res) {
          if (res.status < 500) {
            this.loginError = res.error.description ? res.error.description : res.message;
          } else {
            this.loginError = 'Something went wrong. Please file an issue.';
          }
        }
      }
    );
  }

  toggleShow(show?: boolean) {
    if (show !== undefined) {
      this.show = show;
    } else {
      this.show = this.show ? false : true;
    }

    this.loginForm.reset();
  }

  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }

  hide() {
    this.loginSvc.loginRequest(false);
  }

  ngOnInit() {
    // Initially hide the login box
    this.show = false;

    // Listen from anywhere in the app for events to log in
    this.loginSubscription = this.loginSvc.loginRequests$.subscribe(
      (val) => {
        this.toggleShow(val);
      }
    );

    // Register a login form
    this.loginForm = new FormGroup({
      username: new FormControl(
        '', [
          Validators.required, Validators.minLength(2)
        ]
      ),
      password: new FormControl(
        '', [
          Validators.required, Validators.minLength(8)
        ]
      ),
    });

    // Listen for typing events
    this.loginForm.valueChanges.subscribe(
      () => { this.submitted = false; },
      () => {}
    );
  }

  ngOnDestroy() {
    // Avoid memory leaks with open subscriptions
    this.loginSubscription.unsubscribe();
  }
}
