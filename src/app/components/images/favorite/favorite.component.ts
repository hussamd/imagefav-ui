import { Component, Input, OnInit } from '@angular/core';
import { FavoriteService } from '../../../services/fav.service';
import { LoginService } from '../../../services/login.service';
import { SessionService } from '../../../services/session.service';
import { TagsService } from '../../../services/tags.service';
import { concat, noop } from 'rxjs';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css']
})
export class FavoriteComponent implements OnInit {
  @Input() id: string;
  @Input() favorited: boolean;

  currentTags: string[];
  private inProgress: boolean;

  newTags: string;
  constructor(private favSvc: FavoriteService,
              private loginSvc: LoginService,
              private sessionSvc: SessionService,
              private tagsSvc: TagsService) {
    this.newTags = '';
    this.currentTags = [];
  }

  setUnsetFavorite() {
    if (!this.sessionSvc.isActive()) {
      return this.loginSvc.loginRequest(true);
    }

    if (this.favorited) {
      this.favSvc.remove(this.id).subscribe(
        () => { this.favorited = false; },
        (res) => {
          console.log(res.error);
        }
      );
    } else {
      this.favSvc.add(this.id).subscribe(
        () => { this.favorited = true; },
        (res) => {
          console.log(res.error);
        }
      );
    }
  }

  // added() performs a series of tasks once a given tag is added
  // successfully
  private added(tag: string) {
    if (!this.favorited) {
      return;
    }

    this.newTags = this.newTags.replace(tag, '');
    this.newTags = this.newTags.replace(/ {2,}/g, ' ');

    this.tagsSvc.tagsChange(this.id, this.currentTags.concat(tag));
  }

  // removeTag() removes a single tag from a favorited image
  removeTag(tag: string) {
    this.tagsSvc.remove(this.id, tag).subscribe(
      () => {
        // Send a notifiation which we are also listening to
        // in order to update our data model
        this.currentTags.splice(this.currentTags.indexOf(tag), 1);
        this.tagsSvc.tagsChange(this.id, this.currentTags);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  // Serially add all the requested tags from the `newTags` model.
  // Deduplicate tags that already exist, discard whitespace items
  // and fianlly emit an event onto the tagsChange stream.
  //
  // TODO: Ensure double clicks are prevented.
  addTags() {
    if (this.inProgress === true) {
      return;
    }

    if (!this.sessionSvc.isActive()) {
      return this.loginSvc.loginRequest(true);
    }

    if (!this.favorited) {
      return;
    }

    this.inProgress = true;

    const trimmed = this.newTags.replace(/ {2,}/g, ' ');

    const jobs = [];
    const newTags = this.collateTags(trimmed.split(' '));
    newTags.forEach(
      (tag) => jobs.push(this.tagsSvc.add(this.id, tag))
    );

    let i = 0;
    concat(...jobs).subscribe(
      () => {
        this.added(newTags[i++]);
      },
      (error) => {
        console.log(error);
        this.inProgress = false;
      },
      () => {
        this.inProgress = false;
      }
    );
  }

  // Deduplicate tags from input, and filter out any tags that
  // already exist on the current image object.
  //
  // Return whatever is remaining to be added as tags, wrapped
  // in an observable.
  private collateTags(tags: string[]): string[] {
    return tags.filter(
      (value, index, self) => self.indexOf(value) === index
    ).filter(
      tag => {
        const sanitizedTag = tag.replace(/ /g, '');
        if (!sanitizedTag) { return; }
        if (!this.currentTags || !this.currentTags.includes(sanitizedTag)) {
          return sanitizedTag;
        }
      }
    );
  }

  ngOnInit() {
    // Listen for tag change events - either from us deleting tags
    // or from another component adding new tags.
    //
    // The tags emitted are a list of all the new tags.
    this.tagsSvc.currentTags.subscribe(
      (data) => {
        if (this.id in data) {
          this.currentTags = data[this.id];
        }
      }
    );
  }
}
