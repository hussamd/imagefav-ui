import { Component, OnInit, OnDestroy } from '@angular/core';
import { FavoriteService } from '../../services/fav.service';
import { Observable, Subscription } from 'rxjs';
import { SearchService } from '../../services/search.service';
import { SessionService } from '../../services/session.service';
import { LoginService } from '../../services/login.service';
import { TagsService } from '../../services/tags.service';
import { HttpErrorResponse } from '@angular/common/http';

// Displays all images that come back from SearchService.
//
// Looks up all the images retrieved in a page for being favorited
// and sets up the model appropriately.
@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.css']
})
export class ImagesComponent implements OnInit, OnDestroy {
  favorites: any;
  results: any;
  nextPage: any;
  prevPage: any;
  searchSubscription: Subscription;
  loginSubscription: Subscription;
  error: string;
  loading: boolean;

  constructor(private searchSvc: SearchService,
              private favSvc: FavoriteService,
              private tagsSvc: TagsService,
              private sessionSvc: SessionService,
              private loginSvc: LoginService) {

  }

  stopSpinner() {
    this.loading = false;
  }

  private clearErrors() {
    this.error = '';
  }

  private clearPagination() {
    this.nextPage = this.prevPage = '';
  }

  // forward() initiates an API call to the next page
  forward() {
    if (this.nextPage) {
      const p = this.parseQueryParams(this.nextPage.href);
      this.searchSvc.search(p.q, p.limit, p.offset);
    }
  }

  // backward() initiates an API call to a previous page
  backward() {
    if (this.prevPage) {
      const p = this.parseQueryParams(this.prevPage.href);
      this.searchSvc.search(p.q, p.limit, p.offset);
    }
  }

  // isFavorited() checks if a given image is in our local list
  // of favorites
  isFavorited(imageId: string) {
    return this.favorites !== undefined &&
      this.favorites.some(fav =>
        imageId === fav.image
      );
  }

  // For the given images, query the API for whether or not they
  // have been favorited and have any tags.
  //
  // Upon receiving results, emit events for the tags to update.
  private findFavorites(images: string[]) {
    this.favSvc.areFavorites(images).subscribe(
      (res) => {
        this.favorites = res.favorites;
        // Alert all subscribers of new tags
        this.favorites.forEach(
          (fav) => {
            this.tagsSvc.tagsChange(fav.image, fav.tags);
          }
        );
      }
    );
  }

  private renderResults(res: any) {
    this.results = res.gifs;
    this.nextPage = res._links.next;
    this.prevPage = res._links.prev;
    if (res.gifs.length === 0) {
      this.error = 'No results, try again!';
      this.clearPagination();
    }
    this.stopSpinner();
  }

  // Render the results and if logged in fetch favorite details
  private parseResults(searchObs: Observable<any>) {
    searchObs.subscribe(
      (result) => {
        this.renderResults(result);
        if (this.sessionSvc.isActive()) {
          this.findFavorites(result.gifs.map(gif => gif.id));
        }
      },
      (res: HttpErrorResponse) => {
        this.stopSpinner();
        if (res.status >= 500) {
          this.clearErrors();
          this.error = 'Something went wrong. Please try again shortly...';
        }
        if (res.status < 500) {
          this.clearErrors();
          this.error = res.error.description;
        }
      },
      () => { this.stopSpinner(); }
    );
  }

  // Subscribe to search results to render them, and to login alerts
  // to fetch favorites upon login (if a search is currently rendered)
  ngOnInit() {
    this.searchSubscription = this.searchSvc.searchReturned$.subscribe(
      (searchObs) => {
        this.loading = true;
        this.clearErrors();
        this.parseResults(searchObs);
      }
    );

    // TODO: This should probably be handled differently
    this.loginSubscription = this.loginSvc.loginAlert$.subscribe(
      (loggedIn) => {
        if (loggedIn) {
          if (this.results && this.results.length > 0 && !this.favorites) {
            this.findFavorites(this.results.map(gif => gif.id));
          }
        } else {
          this.favorites = [];
        }
      }
    );
  }

  ngOnDestroy() {
    this.searchSubscription.unsubscribe();
    this.loginSubscription.unsubscribe();
  }

  /**
   * Get the URL parameters
   * source: https://css-tricks.com/snippets/javascript/get-url-variables/
   */
  parseQueryParams(url: string): any {
    const params = {};
    const parser = document.createElement('a');
    parser.href = url;
    const query = parser.search.substring(1);
    const vars = query.split('&');
    for (const item of vars) {
      const pair = item.split('=');
      params[pair[0]] = decodeURIComponent(pair[1]);
    }
    return params;
  }
}
