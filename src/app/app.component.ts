import { NgModule } from '@angular/core';

import { Component } from '@angular/core';

import { NavigationComponent } from './components/navigation/navigation.component';

@NgModule({
  imports: [NavigationComponent],
  declarations: [AppComponent],
  bootstrap: [AppComponent]
})

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {}
