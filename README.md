# ImageFavUi

A demo UI to interact with ImageFav API. Generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

---

### Development setup
1. Install `nodejs` and `npm` as described [here](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

2. If already installed, ensure `npm` is on a recent version:

        npm i npm@latest -g

2. Install `angular-cli` globally:

        npm install -g '@angular/cli'

3. Development server

        ng serve --configuration=staging
        # Runs a dev server here: http://localhost:4200

4. Code scaffolding

        ng generate component component-name
        # For a new component
        # You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

5. Build

        ng build
        # The build artifacts will be stored in the `dist/` directory.
        # Use the `--prod` flag for a production build.

6. Running unit tests

        ng test
        # Execute the unit tests via [Karma](https://karma-runner.github.io).

7. Running end-to-end tests

        ng e2e
        Execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Demo setup
1. Ensure you have npm and angular-cli installed. If not, see [Development setup](#Development setup).

2. Run `ng serve` in the root of the project:

        ng serve
        # Navigate to `http://localhost:4200`
